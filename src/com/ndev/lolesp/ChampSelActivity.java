package com.ndev.lolesp;

import java.util.ArrayList;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

public class ChampSelActivity extends Activity {
	private ChampHelper db;
	ArrayList<Champion> champions;
	ChampListAdapter adapter;
	SearchView search;
	Menu _menu = null;
	boolean rotacion = false;
	boolean counter = false;

	public static String ROTACION = "rotacion";
	public static String COUNTER = "counter";

	private class ChampTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(ChampSelActivity.this);
			progressDialog.setMessage("Cargando campeones");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(Void... dummy) {
			// Creo el helper de la base de datos y recupero los champs
			ChampSelActivity.this.db = new ChampHelper(ChampSelActivity.this);
			champions = db.getChamps(ChampSelActivity.this.rotacion);

			Log.d("Task", "Champs cargados");

			Log.d("Task", "Adapter");
			ChampSelActivity.this.db.close();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if (champions != null) {
				// Busco la lista y le asigno el adapter
				ListView l = (ListView) findViewById(R.id.champ_list);
				adapter = new ChampListAdapter(ChampSelActivity.this, champions);

				MenuItem aux_menu = null;
				if (_menu != null)
					aux_menu = _menu.findItem(R.id.action_search);
				else
					Log.e("Menu", "Menu nulo!");

				if (aux_menu != null) {
					SearchView search = (SearchView) aux_menu.getActionView();
					if (search != null)
						adapter.getFilter().filter(search.getQuery());
				}

				l.setAdapter(adapter);

				l.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {

						Champion selected_champ = (Champion) adapter
								.getItem(position);

						// Inicio la nueva actividad
						Intent myIntent = null;
						if (!ChampSelActivity.this.counter) {
							myIntent = new Intent(ChampSelActivity.this,
									ChampDetail.class);
							myIntent.putExtra(ChampDetail.CHAMP_ID,
									selected_champ.id);
						} else {
							 // Abrimos el listado de counters
							 myIntent = new Intent(ChampSelActivity.this,
							 ChampCounter.class);
							 myIntent.putExtra(ChampCounter.CHAMP_ID,
							 		selected_champ.id);
						}
						
						startActivity(myIntent);

					}
				});

				// Oculto el dialogo
				progressDialog.dismiss();
			} else {
				progressDialog.dismiss();

				Context context = getApplicationContext();
				CharSequence text = "No se pudieron recuperar los campeones";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
				finish();
			}

		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_champ_sel);

		// AdMob adViewSel
		AdView adView = (AdView) this.findViewById(R.id.adViewSel);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		this.rotacion = false;
		Intent i = getIntent();
		if (i != null)
			this.rotacion = i.getBooleanExtra(ChampSelActivity.ROTACION, false);
			this.counter = i.getBooleanExtra(ChampSelActivity.COUNTER, false);

		ChampTask t = new ChampTask();
		t.execute();
		Log.d("Menu", "Finalizando onCreate");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d("Menu", "Entrando al menu");
		// Menu de la ActionBar
		getMenuInflater().inflate(R.menu.champ_sel, menu);
		_menu = menu;

		search = (SearchView) menu.findItem(R.id.action_search).getActionView();

		search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// your text view here
				Log.d("Search", newText);
				ChampListAdapter adap = ChampSelActivity.this.adapter;
				if (adap == null)
					return true;
				Filter f = adap.getFilter();
				if (f != null)
					f.filter(newText);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				return true;
			}
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_ordenar:
			solicitarOrden();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void solicitarOrden() {
		// Arreglo con los ordenes
		final CharSequence[] items = { "Nombre", "IP", "RP", "Ataque",
				"Energia", "Magia", "Dificultad" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Seleccione orden").setItems(items,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Log.d("Diaog", items[which].toString());

						ChampSelActivity.this.adapter.ordenar(which + 1);

					}
				});

		builder.create().show();
	}

}
