package com.ndev.lolesp;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ChampDetail extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	public static String CHAMP_ID = "champ_id";
	private int champ_id = 0;
	private Champion champ = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_champ_detail);
		Log.d("Recreate", "OnCreate");
		
		// AdMob adViewDetail
		AdView adView = (AdView)this.findViewById(R.id.adViewDetail);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest);
		

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		Intent myIntent = getIntent();
		this.champ_id = myIntent.getIntExtra(this.CHAMP_ID, 1);

		if (savedInstanceState != null) {
			// Restore value of members from saved state
			this.champ_id = savedInstanceState.getInt(CHAMP_ID);
		}

		// Creo el champ
		Champion champ = new Champion();
		ChampHelper c_helper = new ChampHelper(ChampDetail.this);
		champ = c_helper.getFullChampByID(this.champ_id);
		this.champ = champ;

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager(), champ);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.

			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state
		savedInstanceState.putInt(CHAMP_ID, this.champ_id);
		Log.d("Recreate", "Se recrea la actividad");
		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		static final public int TAB_DETALLE = 0;
		static final public int TAB_HABILIDAD = 1;
		static final public int TAB_TIPS = 2;

		private Champion c = null;

		private FragmentPpal f_ppal = null;
		private FragmentHab f_hab = null;
		private FragmentTips f_tips = null;

		public SectionsPagerAdapter(FragmentManager fm, Champion c) {
			super(fm);
			this.c = c;

			Log.d("Fragment", "Se crea el pager");
			this.f_ppal = new FragmentPpal();
			this.f_ppal.setChamp(this.c);
			this.f_ppal.setRetainInstance(true);

			this.f_hab = new FragmentHab();
			f_hab.setChamp(this.c);
			this.f_hab.setRetainInstance(true);

			this.f_tips = new FragmentTips();
			f_tips.setChamp(this.c);
			this.f_tips.setRetainInstance(true);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = null;
			if (position == TAB_DETALLE) {
				fragment = f_ppal;
			} else if (position == TAB_HABILIDAD) {
				fragment = f_hab;
			} else if (position == TAB_TIPS) {
				fragment = f_tips;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.title_section1);
			case 1:
				return getString(R.string.title_section2);
			case 2:
				return getString(R.string.title_section3);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class FragmentPpal extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String CHAMP_ID = "id_champ";

		private Champion c = null;

		public FragmentPpal() {
			Log.d("Fragment", "Se contruyo el fragmento Ppal");
		}

		public void setChamp(Champion c) {
			Log.d("Fragment", "Se setea el champ");
			this.c = c;
		}

		public void onStart() {
			super.onStart();
			if (this.c != null)
				Log.d("Start", "Pase por OnStart - No nulo");
			else
				Log.d("Start", "Pase por OnStart - NULO");
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_champ_gral,
					container, false);

			if (c == null) {
				getActivity().finish();
				return rootView;
			}

			// Armo la fuckin vista
			ImageView img = (ImageView) rootView.findViewById(R.id.imgChampion);
			TextView c_name = (TextView) rootView
					.findViewById(R.id.txtChampName);
			TextView c_desc = (TextView) rootView
					.findViewById(R.id.txtChampDesc);

			TextView vida = (TextView) rootView.findViewById(R.id.txtValVida);
			TextView mana = (TextView) rootView.findViewById(R.id.txtValMana);
			TextView ataque = (TextView) rootView
					.findViewById(R.id.txtValAtaque);
			TextView reg_mana = (TextView) rootView
					.findViewById(R.id.txtValRegMana);
			TextView reg_vida = (TextView) rootView
					.findViewById(R.id.txtValRegVida);
			TextView res_magica = (TextView) rootView
					.findViewById(R.id.txtValResMagica);
			TextView vel_ataque = (TextView) rootView
					.findViewById(R.id.txtValVelAtaque);
			TextView vel_movi = (TextView) rootView
					.findViewById(R.id.txtValVelMovi);
			TextView armadura = (TextView) rootView
					.findViewById(R.id.txtValArmadura);

			TextView rp = (TextView) rootView.findViewById(R.id.txtDetalleRP);
			TextView ip = (TextView) rootView.findViewById(R.id.txtDetalleIP);

			ProgressBar health = (ProgressBar) rootView
					.findViewById(R.id.barHealth);
			ProgressBar attack = (ProgressBar) rootView
					.findViewById(R.id.barDetAttack);
			ProgressBar magic = (ProgressBar) rootView
					.findViewById(R.id.barDetMagic);
			ProgressBar diff = (ProgressBar) rootView
					.findViewById(R.id.barDetDiff);
			Log.d("Debyug", c.nombre);
			Context context = img.getContext();
			int id = context.getResources().getIdentifier(c.getImgSkin(0),
					"drawable", context.getPackageName());
			Log.d("Imagen", "Id: " + id);
			img.setImageResource(id);

			// Header
			c_name.setText(this.c.nombre);
			c_desc.setText(this.c.titulo);

			// Detalles
			vida.setText(this.c.vida);
			mana.setText(this.c.mana);
			ataque.setText(this.c.attack_damage);
			reg_mana.setText(this.c.mana_regen);
			reg_vida.setText(this.c.regen);
			res_magica.setText(this.c.res_mag);
			vel_ataque.setText(this.c.attack_speed);
			vel_movi.setText(this.c.mov_speed);
			armadura.setText(this.c.armor);

			rp.setText("" + this.c.rp);
			ip.setText("" + this.c.ip);

			// Barras
			health.setProgress(this.c.health);
			attack.setProgress(this.c.attack);
			magic.setProgress(this.c.magic);
			diff.setProgress(this.c.diff);

			return rootView;
		}
	}

	/**
	 * Fragmento para mostrar las habilidades, es una lista
	 */
	public static class FragmentHab extends Fragment {
		public static final String CHAMP_ID = "id_champ";

		private Champion c = null;
		private ListAdapter adapter = null;

		public FragmentHab() {
		}

		public void setChamp(Champion c) {
			this.c = c;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_champ_hab,
					container, false);
			if (c == null) {
				getActivity().finish();
				return rootView;
			}

			// Busco la lista y le asigno el adapter
			ListView l = (ListView) rootView.findViewById(R.id.lstHabilidades);
			adapter = new ChampHabAdapter(this.getActivity(), this.c);

			l.setAdapter(adapter);

			return rootView;
		}
	}

	/**
	 * Fragmento para mostrar las habilidades, es una lista
	 */
	public static class FragmentTips extends Fragment {
		public static final String CHAMP_ID = "id_champ";

		private Champion c = null;

		public FragmentTips() {
		}

		public void setChamp(Champion c) {
			this.c = c;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_champ_tips,
					container, false);

			if (c == null)
				return rootView;

			// Armo la vista
			TextView aliado = (TextView) rootView
					.findViewById(R.id.txtAliadosDesc);
			TextView enemigo = (TextView) rootView
					.findViewById(R.id.txtEnemigoDesc);

			aliado.setText(Html.fromHtml(this.c.getStrTipAliado()),
					TextView.BufferType.SPANNABLE);
			enemigo.setText(Html.fromHtml(this.c.getStrTipEnemigo()),
					TextView.BufferType.SPANNABLE);

			return rootView;
		}
	}

}
