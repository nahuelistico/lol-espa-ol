package com.ndev.lolesp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CounterListAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<Champion> champions;
	private static LayoutInflater inflater = null;

	public CounterListAdapter(Activity a, ArrayList<Champion> champions) {
		activity = a;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.champions = champions;

	}

	public int getCount() {
		return champions.size();
	}

	public Object getItem(int position) {
		return this.champions.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.counter_item, null);

		ImageView img = (ImageView) vi.findViewById(R.id.imgChamp);
		ProgressBar bar_counter = (ProgressBar) vi.findViewById(R.id.barCounter); 

		Champion champ = champions.get(position);

		// Imagen
		Context context = img.getContext();
		int id = context.getResources().getIdentifier(champ.getImgString(),
				"drawable", context.getPackageName());
		img.setImageResource(id);

		// Barra de progreso del counter
		bar_counter.setProgress(champ.counter);

		return vi;
	}

}
