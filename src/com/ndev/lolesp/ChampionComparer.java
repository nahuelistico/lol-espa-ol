package com.ndev.lolesp;

import java.util.Comparator;

public class ChampionComparer {
	public static Comparator<Champion> getComparer(int field, Boolean desc) {
		
		// Final para poder usar en la clase
		final int f_field = field;
		final Boolean f_desc = desc;

		// Creo el comparator
		Comparator<Champion> ChampionComparator = new Comparator<Champion>() {

			private int _field = f_field;
			private Boolean _desc = f_desc;

			public int compare(Champion c1, Champion c2) {

				return c1.compareTo(c2, _field, _desc);
			}
		};

		return ChampionComparator;

	}
}
