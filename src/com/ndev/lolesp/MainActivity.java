package com.ndev.lolesp;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ndev.lolesp.aux.AppRater;
import com.ndev.lolesp.aux.NDevDialog;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// AdMob
		AdView adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		// Seleccion de campeones
		Button btnCampeones = (Button) findViewById(R.id.btnCampeones);
		btnCampeones.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this,
						ChampSelActivity.class);
				startActivity(myIntent);
			}
		});

		// Rotacion de campeones
		Button btnRotacion = (Button) findViewById(R.id.btnRotacion);
		btnRotacion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this,
						ChampSelActivity.class);
				myIntent.putExtra(ChampSelActivity.ROTACION, true);
				startActivity(myIntent);
			}
		});

		// Reloj de la jungla
		Button btnJungla = (Button) findViewById(R.id.btnJungla);
		btnJungla.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this,
						JungleTimer.class);
				startActivity(myIntent);
			}
		});

		// Ofertas
		Button btnOfertas = (Button) findViewById(R.id.btnOfertas);
		btnOfertas.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this,
						OfertasActivity.class);
				startActivity(myIntent);
			}
		});

		// Rotacion de campeones
		Button btnCounter = (Button) findViewById(R.id.btnCounter);
		btnCounter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this,
						ChampSelActivity.class);
				myIntent.putExtra(ChampSelActivity.COUNTER, true);
				startActivity(myIntent);
			}
		});

		// Rate US!
		AppRater.app_launched(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.acerca_de:
			showAcercaDe();
			return true;
		case R.id.recomendar:
			doRecomendar();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void showAcercaDe() {
		FragmentManager fragmentManager = getFragmentManager();
		NDevDialog newFragment = new NDevDialog();

		newFragment.show(fragmentManager, "dialog");

	}

	public void doRecomendar() {

		String msg = "Hola! Te recomiendo que utilices LoL Español! https://play.google.com/store/apps/details?id=com.ndev.lolesp";

		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
		sendIntent.setType("text/plain");
		startActivity(Intent.createChooser(sendIntent, "Recomendarnos"));

	}
}
