package com.ndev.lolesp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ChampListAdapter extends BaseAdapter implements Filterable {

	private Activity activity;
	private ArrayList<Champion> champions, original_champions;
	private static LayoutInflater inflater = null;

	private Filter filter;

	public ChampListAdapter(Activity a, ArrayList<Champion> champions) {
		activity = a;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.champions = champions;
		this.original_champions = champions;

		filter = new Filter() {

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				
				// Recorro todos los nombres filtrando
				ArrayList<Champion> aux = new ArrayList<Champion>();
				int i = 0;
				String cmp = constraint.toString().toLowerCase();
				while (i < ChampListAdapter.this.original_champions.size()) {
					if (ChampListAdapter.this.original_champions.get(i).nombre.toLowerCase()
							.contains(cmp)) {
						aux.add(ChampListAdapter.this.original_champions.get(i).copiar());
					}
					i++;
				}
				ChampListAdapter.this.champions = aux;
				return null;
			}
		};

	}

	public int getCount() {
		return champions.size();
	}

	public Object getItem(int position) {
		return this.champions.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.champ_item, null);

		ImageView img = (ImageView) vi.findViewById(R.id.imgChamp);
		TextView nombre = (TextView) vi.findViewById(R.id.txtNombre);
		TextView ip = (TextView) vi.findViewById(R.id.txtIP);
		TextView rp = (TextView) vi.findViewById(R.id.txtRPOferta);
		ProgressBar health = (ProgressBar) vi.findViewById(R.id.barHealth);
		ProgressBar attack = (ProgressBar) vi.findViewById(R.id.barAttack);
		ProgressBar magic = (ProgressBar) vi.findViewById(R.id.barMagic);
		ProgressBar diff = (ProgressBar) vi.findViewById(R.id.barDiff);

		Champion champ = champions.get(position);

		// Imagen
		Context context = img.getContext();
		int id = context.getResources().getIdentifier(champ.getImgString(),
				"drawable", context.getPackageName());
		img.setImageResource(id);

		// Datos ppales
		nombre.setText(champ.nombre);
		ip.setText(String.valueOf(champ.ip));
		rp.setText(String.valueOf(champ.rp));

		// Propiedades
		health.setProgress(champ.health);
		attack.setProgress(champ.attack);
		magic.setProgress(champ.magic);
		diff.setProgress(champ.diff);

		return vi;
	}

	@Override
	public Filter getFilter() {
		return filter;
	}
	
	public void ordenar(int campo){
		Comparator<Champion> c = ChampionComparer.getComparer(campo, true);
		Collections.sort(champions, c);
		Collections.sort(original_champions, c);
		notifyDataSetChanged();
	}
}
