package com.ndev.lolesp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class ChampHelper extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "lesp.db";
	private static final int DATABASE_VERSION = 25;

	public ChampHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		// you can use an alternate constructor to specify a database location
		// (such as a folder on the sd card)
		// you must ensure that this folder is available and you have permission
		// to write to it
		// super(context, DATABASE_NAME,
		// context.getExternalFilesDir(null).getAbsolutePath(), null,
		// DATABASE_VERSION);

		setForcedUpgrade();

	}

	public ArrayList<Champion> getChamps(boolean rotation) {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String[] sqlSelect = { "id _id", "nombre", "ip", "rp", "rank_health",
				"rank_attack", "rank_magic", "rank_dif" };
		String sqlTables = "Champion";

		qb.setTables(sqlTables);

		String where = null;

		if (rotation) {
			Log.d("Rotacion", "Buscando rotacion");
			String in = getStream("http://api.ndev.com.ar/rotacion.php");

			if (in == null)
				return null;

			where = " id IN (" + in + ")";
			Log.d("Rotacion", "WHERE: " + where);

		}

		Cursor c = qb.query(db, sqlSelect, where, null, null, null, "nombre");

		ArrayList<Champion> champions = new ArrayList<Champion>();

		while (c.moveToNext()) {
			Champion aux_champ = new Champion();
			aux_champ.id = c.getInt(0);
			aux_champ.nombre = c.getString(1);
			aux_champ.ip = c.getInt(2);
			aux_champ.rp = c.getInt(3);
			aux_champ.health = c.getInt(4);
			aux_champ.attack = c.getInt(5);
			aux_champ.magic = c.getInt(6);
			aux_champ.diff = c.getInt(7);
			champions.add(aux_champ);
		}
		return champions;
	}

	public Champion getFullChampByID(int id) {

		// Manejadores
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		// Campos
		String[] sqlSelect = { "id", "nombre", "ip", "rp", "rank_health",
				"rank_attack", "rank_magic", "rank_dif", "afiliacion", "vida",
				"mana", "attack_damage", "attack_speed", "mov_speed", "regen",
				"mana_regen", "armor", "res_mag", "titulo" };
		String sqlTables = "Champion";

		qb.setTables(sqlTables);
		Cursor c = qb
				.query(db, sqlSelect, "id = " + id, null, null, null, null);

		Champion champ = new Champion();

		// Header
		while (c.moveToNext()) {
			champ.id = c.getInt(0);
			champ.nombre = c.getString(1);
			champ.ip = c.getInt(2);
			champ.rp = c.getInt(3);
			champ.health = c.getInt(4);
			champ.attack = c.getInt(5);
			champ.magic = c.getInt(6);
			champ.diff = c.getInt(7);

			champ.afiliacion = c.getString(8);
			champ.vida = c.getString(9);
			champ.mana = c.getString(10);
			champ.attack_damage = c.getString(11);
			champ.attack_speed = c.getString(12);
			champ.mov_speed = c.getString(13);
			champ.regen = c.getString(14);
			champ.mana_regen = c.getString(15);
			champ.armor = c.getString(16);
			champ.res_mag = c.getString(17);

			champ.titulo = c.getString(18);
		}
		c.close();
		Log.d("Query", champ.nombre);

		// Habilidades
		String[] sqlHab = { "hab", "nombre", "costo", "alcance", "cooldown",
				"img", "desc_1", "desc_2" };
		sqlTables = "champion_hab";

		qb.setTables(sqlTables);
		c = qb.query(db, sqlHab, "id = " + id, null, null, null, null);

		while (c.moveToNext()) {
			Habilidad h = new Habilidad();
			h.hab = c.getInt(0);
			h.nombre = c.getString(1);
			h.costo = c.getString(2);
			h.alcance = c.getString(3);
			h.cooldown = c.getString(4);
			h.img = c.getString(5);
			h.desc_1 = c.getString(6);
			h.desc_2 = c.getString(7);
			champ.habilidades[h.hab] = h;
		}
		c.close();

		// Skins
		String[] sqlSkin = { "id_skin", "nombre", "archivo" };
		sqlTables = "champion_skin";

		qb.setTables(sqlTables);
		c = qb.query(db, sqlSkin, "id = " + id, null, null, null, null);

		while (c.moveToNext()) {
			Skin s = new Skin();
			s.id = c.getInt(0);
			s.nombre = c.getString(1);
			s.img = c.getString(2);

			champ.skins[s.id] = s;
		}
		c.close();

		// Tips
		String[] sqlTip = { "tipo", "descripcion" };
		sqlTables = "champion_tip";

		qb.setTables(sqlTables);
		c = qb.query(db, sqlTip, "id = " + id, null, null, null, null);

		int count_ali = 0;
		int count_enemy = 0;
		while (c.moveToNext()) {

			int tipo = c.getInt(0);
			String tip = c.getString(1);
			if (tipo == 0) {
				champ.tips_aliados[count_ali] = tip;
				count_ali++;
			} else {
				champ.tips_enemig[count_enemy] = tip;
				count_enemy++;
			}
		}
		c.close();

		return champ;
	}

	public LolTimer getTimer(String nombre) {

		// Manejadores
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		// Campos
		String[] sqlSelect = { "segundos", "img_active", "img_dead" };
		String sqlTables = "timer";

		qb.setTables(sqlTables);
		Cursor c = qb.query(db, sqlSelect, "nombre = '" + nombre + "'", null,
				null, null, null);

		LolTimer t = null;
		// Header
		while (c.moveToNext()) {
			t = new LolTimer(nombre, c.getInt(0), c.getString(1),
					c.getString(2));
		}
		c.close();

		return t;
	}

	private String getStream(String page) {
		try {
			URL url = new URL(page);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setConnectTimeout(1000);
			return convertStreamToString(urlConnection.getInputStream());
		} catch (Exception ex) {
			Log.d("URL", ex.toString());
			return null;
		}
	}

	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public ArrayList<Champion> getCounters(int id_champion) {
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		// Recuperamos los counters
		String[] sqlSelect = { "id _id", "id_counter", "tipo", "valor" };
		String sqlTables = "champion_counter";

		qb.setTables(sqlTables);

		
		Cursor c = qb.query(db, sqlSelect, "id = " + id_champion, null, null,
				null, "valor DESC");
		Log.d("Counter", "Counters de " + id_champion);

		ArrayList<Champion> champions = new ArrayList<Champion>();
		while (c.moveToNext()) {
			Champion aux_champ = new Champion();
			aux_champ.id = c.getInt(1);
			aux_champ.counter_fuerte = c.getInt(2) == 1;
			aux_champ.counter = c.getInt(3);
			champions.add(aux_champ);

			Log.d("Counter", "" + aux_champ.id);
		}
		c.close();

/*		// Cargamos lo que nos falta (nombre)
		for (int i = 0; i < champions.size(); i++) {
			// Datos que faltan
			String[] sqlDatos = { "nombre" };
			sqlTables = "champion";
			Champion champ = champions.get(i);
			int id = champ.id;

			qb.setTables(sqlTables);
			c = qb.query(db, sqlDatos, "id = " + id, null, null, null, null);

			while (c.moveToNext()) {
				champ.nombre = c.getString(0);

				Log.d("Counter", champ.nombre);
			}
			c.close();

		}
*/
		return champions;
	}

	public String getChampName(int id_champion) {
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		String res = "";

		// Recuperamos los counters
		String[] sqlSelect = { "id _id", "nombre" };
		String sqlTables = "champion";

		qb.setTables(sqlTables);

		
		Cursor c = qb.query(db, sqlSelect, "id = " + id_champion, null, null,
				null, "");
		Log.d("Nombre", "Nombre de " + id_champion);

		ArrayList<Champion> champions = new ArrayList<Champion>();
		while (c.moveToNext()) {
			res = c.getString(1);
		}
		c.close();
		return res;
	}

}
