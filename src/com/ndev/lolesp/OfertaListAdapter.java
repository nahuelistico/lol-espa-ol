package com.ndev.lolesp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ndev.lolesp.aux.Oferta;

public class OfertaListAdapter extends BaseAdapter {
	ArrayList<Oferta> ofertas = null;

	LayoutInflater inflater = null;

	public OfertaListAdapter(Activity a, ArrayList<Oferta> ofertas) {
		this.ofertas = ofertas;
		if (ofertas == null)
				return;
		Log.d("Oferta", "Se cargaron " + ofertas.size() + " ofertas");
		inflater = (LayoutInflater) a
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return this.ofertas.size();
	}

	@Override
	public Object getItem(int position) {
		return this.ofertas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.oferta, null);

		Oferta o = (Oferta) this.getItem(position);
		// Recupero los elementos
		TextView descripcion = (TextView) vi.findViewById(R.id.txtOfertaItem);
		ImageView img = (ImageView) vi.findViewById(R.id.imgOferta);
		TextView rp = (TextView) vi.findViewById(R.id.txtRPOferta);

		// Seteo el RP
		rp.setText(String.format("%d", o.rp));
		descripcion.setText(o.desc);

		
		if (!o.url && o.bitmap == null) {
			// Cargo el bitmap por primera vez (resource)
			Context context = img.getContext();
			int id = context.getResources().getIdentifier(o.resource_str,
					"drawable", context.getPackageName());
			o.bitmap = BitmapFactory.decodeResource(context.getResources(), id);
		}

		// Seteo la imagen
		if (o.bitmap != null) {
			img.setImageBitmap(o.bitmap);
			vi.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
		} else {
			img.setImageBitmap(null);
			vi.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
		}
		return vi;
	}
}
