package com.ndev.lolesp;

import java.util.ArrayList;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

public class ChampCounter extends Activity {
	protected static final String CHAMP_ID = "counter_id";
	private ChampHelper db;
	ArrayList<Champion> champions;
	String titulo = "";

	int id_champion = 0;

	private class ChampTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(ChampCounter.this);
			progressDialog.setMessage("Cargando campeones");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(Void... dummy) {
			// Creo el helper de la base de datos y recupero los champs
			ChampCounter.this.db = new ChampHelper(ChampCounter.this);
			champions = db.getCounters(ChampCounter.this.id_champion);
			titulo = db.getChampName(ChampCounter.this.id_champion);

			Log.d("Task", "Champs cargados");

			Log.d("Task", "Adapter");
			ChampCounter.this.db.close();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			// Titulo
			if (titulo != "")
				ChampCounter.this.setTitle("Counters de " + titulo);

			// Listado
			if (champions != null) {
				ArrayList<Champion> c_fuertes = new ArrayList<Champion>();
				ArrayList<Champion> c_debiles = new ArrayList<Champion>();

				for (int i = 0; i < champions.size(); i++) {
					Champion c = champions.get(i);
					if (c.counter_fuerte)
						c_fuertes.add(c);
					else
						c_debiles.add(c);
				}

				// Busco la lista y le asigno los champions fuertes
				ListView fuertes = (ListView) findViewById(R.id.lst_fuerte);
				CounterListAdapter adapter = new CounterListAdapter(
						ChampCounter.this, c_fuertes);
				fuertes.setAdapter(adapter);

				// Debiles
				ListView debil = (ListView) findViewById(R.id.lst_debil);
				CounterListAdapter adapter_debil = new CounterListAdapter(
						ChampCounter.this, c_debiles);
				debil.setAdapter(adapter_debil);

				// Oculto el dialogo
				progressDialog.dismiss();
			} else {
				progressDialog.dismiss();

				Context context = getApplicationContext();
				CharSequence text = "No se pudieron recuperar los counters";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
				finish();
			}

		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_champ_counter);

		// AdMob adViewSel
		AdView adView = (AdView) this.findViewById(R.id.adViewSel);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		// Recuperamos el id del champion
		Intent i = getIntent();
		if (i != null)
			this.id_champion = i.getIntExtra(ChampCounter.CHAMP_ID, 0);

		ChampTask t = new ChampTask();
		t.execute();
		Log.d("Menu", "Finalizando onCreate");
	}
}
