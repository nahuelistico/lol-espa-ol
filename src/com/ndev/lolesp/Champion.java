package com.ndev.lolesp;

public class Champion {

	public static final int CAMPO_ID = 0;
	public static final int CAMPO_NOMBRE = 1;
	public static final int CAMPO_IP = 2;
	public static final int CAMPO_RP = 3;
	public static final int CAMPO_ATAQUE = 4;
	public static final int CAMPO_ENERGIA = 5;
	public static final int CAMPO_MAGIA = 6;
	public static final int CAMPO_DIFICULTAD = 7;

	public int id = 0;
	public String nombre = "";
	public int ip = 0;
	public int rp = 0;
	public int attack = 0;
	public int health = 0;
	public int diff = 0;
	public int magic = 0;
	
	public int counter = 0; // Solo se usa para la lista de counters!
	public boolean counter_fuerte = false;
	

	public String afiliacion = "";
	public String vida = "";
	public String mana = "";
	public String attack_damage = "";
	public String attack_speed = "";
	public String mov_speed = "";
	public String regen = "";
	public String mana_regen = "";
	public String armor = "";
	public String res_mag = "";

	public String titulo = "";

	// Habilidades
	public Habilidad[] habilidades = new Habilidad[5];

	// Skins
	public Skin[] skins = new Skin[20];

	// Tips
	public String[] tips_aliados = new String[10];
	public String[] tips_enemig = new String[10];

	public String getImgString() {
		return "le_" + this.id + "_img";
	}

	public Champion copiar() {
		Champion aux = new Champion();
		aux.id = this.id;
		aux.nombre = this.nombre;
		aux.ip = this.ip;
		aux.rp = this.rp;
		aux.attack = this.attack;

		aux.health = this.health;
		aux.diff = this.diff;

		aux.magic = this.magic;

		return aux;
	}
	
	private String _getStrTip(String[] tips){
		String res = "";
		for(int i = 0; i<tips.length;i++){
			String tip = tips[i];
			if (tip == null)
					break;
			res += "- "+tip+"<br/>";
		}
		return res;
	}
	
	public String getStrTipAliado(){
		return this._getStrTip(this.tips_aliados);
	}
	public String getStrTipEnemigo(){
		return this._getStrTip(this.tips_enemig);
	}
	
	

	public int compareTo(Champion c, int field, Boolean desc) {
		int res = 0;

		int val1 = 01, val2 = 0;

		if (field != Champion.CAMPO_NOMBRE) {

			// Recupero los valores a comprar
			switch (field) {
			case Champion.CAMPO_ID:
				val1 = this.id;
				val2 = c.id;
				break;

			case Champion.CAMPO_IP:
				val1 = this.ip;
				val2 = c.ip;
				break;

			case Champion.CAMPO_RP:
				val1 = this.rp;
				val2 = c.rp;
				break;

			case Champion.CAMPO_ATAQUE:
				val1 = this.attack;
				val2 = c.attack;
				break;

			case Champion.CAMPO_ENERGIA:
				val1 = this.health;
				val2 = c.health;
				break;

			case Champion.CAMPO_MAGIA:
				val1 = this.magic;
				val2 = c.magic;
				break;
			case Champion.CAMPO_DIFICULTAD:
				val1 = this.diff;
				val2 = c.diff;
				break;
			}

			// Enteros
			if (val1 > val2)
				res = 1;
			else if (val1 < val2)
				res = -1;
		} else {
			res = this.nombre.compareTo(c.nombre);
		}
		if (desc)
			res *= -1;
		return res;
	}

	public String getImgSkin(int i) {
		if (i == 0) {
			return "skn_" + this.id + "000";
		}
		return "";
	}
	
}
