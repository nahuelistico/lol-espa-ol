package com.ndev.lolesp;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChampHabAdapter extends BaseAdapter {

	private Activity activity;
	private Champion champion;
	private static LayoutInflater inflater = null;

	public ChampHabAdapter(Activity a, Champion champion) {
		activity = a;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.champion = champion;
	}

	public int getCount() {
		return 5;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.fragment_hab_detalle, null);

		// Recupero la habilidad actual
		Habilidad h = this.champion.habilidades[position];

		ImageView img = (ImageView) vi.findViewById(R.id.imgHabilidad);
		TextView nombre = (TextView) vi.findViewById(R.id.txtHabilidad);

		TextView costo = (TextView) vi.findViewById(R.id.txtValCosto);
		TextView cooldown = (TextView) vi.findViewById(R.id.txtValCooldown);

		TextView descripcion = (TextView) vi.findViewById(R.id.txtDescripcion);

		// Seteo los campos
		nombre.setText(h.nombre);
		if (h.costo.equals("0") || h.costo.equals(""))
			costo.setText("Sin costo");
		else
			costo.setText(h.costo);

		if (h.cooldown.equals("0") || h.cooldown.equals(""))
			cooldown.setText("Sin cooldown");
		else
			cooldown.setText(h.cooldown);

		descripcion.setText(Html.fromHtml(h.getDescripcion()),
				TextView.BufferType.SPANNABLE);

		Context context = img.getContext();
		int id = context.getResources().getIdentifier(
				h.getImage(this.champion.id), "drawable",
				context.getPackageName());
		Log.d("Imagen", "Id: " + id);
		img.setImageResource(id);

		return vi;
	}
}
