package com.ndev.lolesp.aux;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.ndev.lolesp.R;

/**
 * 
 */
public class NDevDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View v = inflater.inflate(R.layout.ndev_dialog, null);

		builder.setView(v);

		// Asocio los eventos a las imagenes
		ImageView facebook = (ImageView) v.findViewById(R.id.iconFb);
		ImageView twitter = (ImageView) v.findViewById(R.id.iconTw);
		ImageView play = (ImageView) v.findViewById(R.id.iconPlay);

		facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent browserIntent = null;
				try {
					v.getContext().getPackageManager()
							.getPackageInfo("com.facebook.katana", 0);
					browserIntent = new Intent(Intent.ACTION_VIEW,
							Uri.parse("fb://page/346535268819569"));
				} catch (Exception e) {
					browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("https://www.facebook.com/nDevDesarrollos"));
				}

				startActivity(browserIntent);
			}
		});
		twitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent browserIntent = null;
				try {
					browserIntent = new Intent(Intent.ACTION_VIEW,
							Uri.parse("twitter://user?screen_name=ndevdesarrollos"));
					startActivity(browserIntent);
				} catch (Exception e) {
					browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("https://www.twitter.com/nDevDesarrollos"));
					startActivity(browserIntent);
				}

				
			}
		});
		play.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://search?q=pub:nDev Desarrollos"));
				startActivity(intent);
			}
		});

		// Create the AlertDialog object and return it
		return builder.create();
	}

}
