package com.ndev.lolesp.aux;

import java.io.InputStream;
import java.lang.ref.WeakReference;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.BaseAdapter;

public class AsyncImage extends AsyncTask<String, Void, Bitmap> {

	private final WeakReference<BaseAdapter> adapter;
	private final WeakReference<Oferta> oferta;

	public AsyncImage(Oferta o, BaseAdapter adapter) {
		this.oferta = new WeakReference<Oferta>(o);
		this.adapter = new WeakReference<BaseAdapter>(adapter);
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected Bitmap doInBackground(String... urls) {
		String urldisplay = urls[0];
		Bitmap bitmap = null;
		try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			bitmap = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		if (result != null) {
			this.oferta.get().bitmap = result;
			this.adapter.get().notifyDataSetChanged();
		}
	};

}
