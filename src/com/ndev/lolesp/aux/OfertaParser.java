package com.ndev.lolesp.aux;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class OfertaParser {

	String url = null;

	public OfertaParser(String url) {

		this.url = url;

	}

	public ArrayList<Oferta> getOfertas() {

		// Recupero el json de la pagina
		String json = getStream(url);
		if (json == null) {
			Log.e("JSON", "No se pudo recuperar el json de " + url);
			return null;
		}

		ArrayList<Oferta> ofertas = new ArrayList<Oferta>();

		// Recorro el arreglo
		try {

			JSONArray root = new JSONArray(json);
			JSONObject oferta = null;
			for (int i = 0; i < root.length(); i++) {
				oferta = (JSONObject) root.getJSONObject(i);

				Oferta o = new Oferta();
				o.champ_id = oferta.getInt("id");
				o.rp = oferta.getInt("rp");
				o.desc = oferta.getString("desc");

				String img = oferta.getString("skn");

				o.img = img;
				o.resource_str = "skn_" + o.champ_id + "000";
				o.url = img.compareTo("0") != 0;
				ofertas.add(o);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ofertas;
	}

	private String getStream(String page) {
		try {
			URL url = new URL(page);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setConnectTimeout(5000);
			return convertStreamToString(urlConnection.getInputStream());
		} catch (Exception ex) {
			Log.d("URL", ex.toString());
			return null;
		}
	}

	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
