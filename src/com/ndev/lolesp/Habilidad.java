package com.ndev.lolesp;

public class Habilidad {
	public int hab = 0;
	public String nombre = "";
	public String costo = "";
	public String alcance = "";
	public String cooldown = "";
	public String img = "";
	public String desc_1 = "";
	public String desc_2 = "";

	public String getImage(int id_champ) {
		String end = "";
		if (hab == 0)
			end = "passive";
		else if (hab == 1)
			end = "q";
		else if (hab == 2)
			end = "w";
		else if (hab == 3)
			end = "e";
		else if (hab == 4)
			end = "r";
		return "h_" + id_champ + "_" + end;
	}

	public String getDescripcion() {
		//String aux_1 = desc_1.replace("Pasiva: ", "<br/>Pasiva: ").replace(
		//		"Activa:", "<br/>Activa:");
		//String aux_2 = desc_2.replace("Pasiva:", "<br/>Pasiva: ").replace(
		//		"Activa:", "<br/>Activa: ");
		return desc_1 + "<br/> <br/>" + desc_2;
	}

	public String getLetra() {
		String letra = "";
		if (hab == 0)
			letra = "Pasiva";
		else if (hab == 1)
			letra = "Q";
		else if (hab == 2)
			letra = "W";
		else if (hab == 3)
			letra = "E";
		else if (hab == 4)
			letra = "R";
		return letra;
	}
}
