package com.ndev.lolesp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

public class DialogOrden extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		// Arreglo con los ordenes
		final CharSequence[] items = { "Nombre", "IP", "RP", "Energia", "Ataque",
				"Magia", "Dificultad" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Seleccione orden").setItems(items,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Log.d("Diaog", items[which].toString());
					}
				});
		return builder.create();
	}
}
