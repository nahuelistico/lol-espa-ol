package com.ndev.lolesp;

import java.util.Timer;
import java.util.TimerTask;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Reloj contador, recibe una cantidad de milisegundos a contar
 * 
 * @author nfilipuzzi
 */
public class Reloj implements OnClickListener {

	public int milisegundos;
	public int milisegundos_original;

	private Button btn = null;
	private boolean corriendo = false;
	private CountDownTimer countdown = null;

	private LolTimer t = null;
	public MediaPlayer sound = null;


	/**
	 * Constructor de la clase
	 * 
	 * @param m
	 *            Milisegundos que cuenta el reloj
	 */
	public Reloj(LolTimer t, Button button, MediaPlayer mp) {
		milisegundos = t.segundos * 1000;
		milisegundos_original = milisegundos;
		corriendo = false;
		
		btn = button;
		btn.setBackgroundResource(t.getImgActive(btn));
		
		this.sound = mp;
		this.t = t;

		btn.setOnClickListener(this);
		btn.setText(this.getTime());
	}

	/**
	 * Retorna un string con el tiempo restante
	 * 
	 * @return String con el tiempo de la forma mm:ss
	 */
	public String getTime() {
		int segundos = milisegundos / 1000;
		return String.format("%d:%02d", segundos / 60, segundos % 60);
	}

	@Override
	public void onClick(View v) {
		Log.i("JungleTimer", "Botton presionado");

		// Si estoy corriendo, reseteo todo
		if (corriendo) {
			if (countdown != null) {
				countdown.cancel();
			}
			milisegundos = milisegundos_original;
			btn.setText(getTime());
		} else {
			// Si no, seteo el background
			btn.setBackgroundResource(t.getImgDead(btn));
		}
		btn.setBackgroundResource(t.getImgDead(btn));
		
		countdown = new CountDownTimer(milisegundos_original, 1000) {

			public void onTick(long millisUntilFinished) {
				milisegundos -= 1000;
				btn.setText(getTime());
			}

			public void onFinish() {
				btn.setBackgroundResource(t.getImgActive(btn));
				milisegundos = milisegundos_original;
				btn.setText(getTime());
				corriendo = false;
				Reloj.this.sound.start();
			}
		};
		corriendo = true;
		countdown.start();

	}
}// end Reloj