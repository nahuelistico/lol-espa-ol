package com.ndev.lolesp;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.Button;

public class JungleTimer extends Activity {

	// Top
	private Button btnTopWraithBig = null;
	private Button btnTopWraithSmall = null;
	private Button btnTopWolf = null;
	private Button btnTopGollem = null;

	// Bottom
	private Button btnBottomWraithBig = null;
	private Button btnBottomWraithSmall = null;
	private Button btnBottomWolf = null;
	private Button btnBottomGollem = null;

	// Grander
	private Button btnBaron = null;
	private Button btnDragon = null;
	private Button btnRedTop = null;
	private Button btnRedBottom = null;
	private Button btnBlueTop = null;
	private Button btnBlueBottom = null;

	// Relojes!
	private Reloj rTopWraithBig = null;
	private Reloj rTopWraithSmall = null;
	private Reloj rTopWolf = null;
	private Reloj rTopGollem = null;

	private Reloj rBottomWraithBig = null;
	private Reloj rBottomWraithSmall = null;
	private Reloj rBottomWolf = null;
	private Reloj rBottomGollem = null;

	private Reloj rBaron = null;
	private Reloj rDragon = null;
	private Reloj rRedTop = null;
	private Reloj rRedBottom = null;
	private Reloj rBlueTop = null;
	private Reloj rBlueBottom = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jungle_timer);
		
		// AdMob
		AdView adView = (AdView) this.findViewById(R.id.adViewReloj);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		// Recupero todos los botones... sick!
		btnTopWraithBig = (Button) findViewById(R.id.btnTopWraithBig);
		btnTopWraithSmall = (Button) findViewById(R.id.btnTopWraithSmall);
		btnTopWolf = (Button) findViewById(R.id.btnTopWolf);
		btnTopGollem = (Button) findViewById(R.id.btnTopGollem);

		btnBottomWraithBig = (Button) findViewById(R.id.btnBottomWraithBig);
		btnBottomWraithSmall = (Button) findViewById(R.id.btnBottomWraithSmall);
		btnBottomWolf = (Button) findViewById(R.id.btnBottomWolf);
		btnBottomGollem = (Button) findViewById(R.id.btnBottomGollem);

		btnBaron = (Button) findViewById(R.id.btnBaron);
		btnDragon = (Button) findViewById(R.id.btnDragon);
		btnRedTop = (Button) findViewById(R.id.btnRedTop);
		btnRedBottom = (Button) findViewById(R.id.btnRedBottom);
		btnBlueTop = (Button) findViewById(R.id.btnBlueTop);
		btnBlueBottom = (Button) findViewById(R.id.btnBlueBottom);

		// Recupero todos los timers de la base
		ChampHelper db = new ChampHelper(this);
		MediaPlayer mp = MediaPlayer.create(getApplicationContext(),
				R.raw.jungle);

		// Creo los timers con cada bottom
		rTopWraithBig = new Reloj(db.getTimer("flash"), btnTopWraithBig, mp);
		rTopWraithSmall = new Reloj(db.getTimer("flash"), btnTopWraithSmall,
				mp);
		rTopWolf = new Reloj(db.getTimer("flash"), btnTopWolf, mp);
		rTopGollem = new Reloj(db.getTimer("flash"), btnTopGollem, mp);

		rBottomWraithBig = new Reloj(db.getTimer("gromp"), btnBottomWraithBig,
				mp);
		rBottomWraithSmall = new Reloj(db.getTimer("wraith"),
				btnBottomWraithSmall, mp);
		rBottomWolf = new Reloj(db.getTimer("wolf"), btnBottomWolf, mp);
		rBottomGollem = new Reloj(db.getTimer("golem_small"), btnBottomGollem,
				mp);

		rBaron = new Reloj(db.getTimer("baron"), btnBaron, mp);
		rDragon = new Reloj(db.getTimer("dragon"), btnDragon, mp);
		rRedTop = new Reloj(db.getTimer("lizard"), btnRedTop, mp);
		rRedBottom = new Reloj(db.getTimer("lizard"), btnRedBottom, mp);
		rBlueTop = new Reloj(db.getTimer("golem"), btnBlueTop, mp);
		rBlueBottom = new Reloj(db.getTimer("golem"), btnBlueBottom, mp);

		db.close();

	}

}
