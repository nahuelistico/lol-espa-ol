package com.ndev.lolesp;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Button;

public class LolTimer {
	public String nombre = "";
	public int segundos = 0;
	public String img_active = "";
	public String img_dead = "";

	public LolTimer(String nombre, int segundos, String img_active,
			String img_dead) {
		this.nombre = nombre;
		this.segundos = segundos;
		this.img_active = img_active.substring(0, img_active.length() - 4);
		this.img_dead = img_dead.substring(0, img_dead.length() - 4);
		
	}

	public int getImgActive(Button btn) {
		Context context = btn.getContext();
		int id = context.getResources().getIdentifier(this.img_active,
				"drawable", context.getPackageName());
		Log.d("Timer", "Id:" + id);
		return id;
	}

	public int getImgDead(Button btn) {
		Context context = btn.getContext();
		int id = context.getResources().getIdentifier(this.img_dead,
				"drawable", context.getPackageName());
		Log.d("Timer", "Id:" + id);
		return id;
	}

}
