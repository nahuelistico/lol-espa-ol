package com.ndev.lolesp;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ndev.lolesp.aux.AsyncImage;
import com.ndev.lolesp.aux.OfertaParser;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.ndev.lolesp.aux.Oferta;

public class OfertasActivity extends Activity {

	OfertaListAdapter adapter = null;
	ListView l = null;
	ArrayList<AsyncImage> imagenes = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ofertas);

		// AdMob adViewOferta
		AdView adView = (AdView) this.findViewById(R.id.adViewOferta);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		// Guardo las imagenes parapoder cancelarlas en caso de ser necesario
		imagenes = new ArrayList<AsyncImage>();

		// Creo la lista y le agrego el adapter
		this.l = (ListView) findViewById(R.id.lstOfertas);

		OfertaTask t = new OfertaTask();
		t.execute("http://api.ndev.com.ar/oferta.php");
	}

	private class OfertaTask extends AsyncTask<String, Void, ArrayList<Oferta>> {
		ProgressDialog progressDialog = null;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(OfertasActivity.this);
			progressDialog.setMessage("Cargando ofertas...");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected ArrayList<Oferta> doInBackground(String... urls) {
			// Parser de las ofertas
			OfertaParser parser = new OfertaParser(urls[0]);
			return parser.getOfertas();
		}

		@Override
		protected void onPostExecute(ArrayList<Oferta> result) {

			if (result != null) {
				OfertasActivity.this.adapter = new OfertaListAdapter(
						OfertasActivity.this, result);
				OfertasActivity.this.l.setAdapter(OfertasActivity.this.adapter);

				// Empiezo a cargar las imagenes
				for (int i = 0; i < result.size(); i++) {
					if (result.get(i).url)
						loadBitmap(result.get(i));
				}
			} else {
				Context context = getApplicationContext();
				CharSequence text = "No se pudieron recuperar los ofertas";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
				finish();
				return;
			}
			// chau dialogo
			progressDialog.dismiss();
		};

		public void loadBitmap(Oferta o) {
			AsyncImage task = new AsyncImage(o, OfertasActivity.this.adapter);
			task.execute(o.img);
			OfertasActivity.this.imagenes.add(task);
		}

	}

	@Override
	protected void onDestroy() {
		// Cancelo las tareas de descarga de imagenes
		for (int i = 0; i < imagenes.size(); i++) {
			imagenes.get(i).cancel(true);
		}
		super.onDestroy();
	}

}
